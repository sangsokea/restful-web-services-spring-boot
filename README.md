# Spring Boot Demo

### Content
1. RESTful Web Services
2. Spring Boot Security
3. Other Resources

## 1. RESTful Web Services

Please keep up your hard work. It's almost over.

### OpenAPI 3 Url:

<http://localhost:8080/swagger-ui/index.html>

### Swagger UI Customization

Change Bean configurations in the file below.

```shell
src/main/java/com/ksga/restdemo/config/OpenApiConfig.java
```

### Database Scripts

Don't forget to create a database and update your `application.properties` file. 

Then run the scripts below.

```shell
src/main/resources/scripts/schema.sql
src/main/resources/scripts/data.sql
```

### ReactJS Project:

This is the ReactJS project used to fetch data from the REST API.
Please clone then run 

<https://gitlab.com/eam.dayan/react-demo-for-rest-api.git>

```shell
git clone https://gitlab.com/eam.dayan/react-demo-for-rest-api.git 
cd react-demo-for-rest-api
npm install
npm start
```

## 2. Spring Security 
### Spring Security Thymeleaf

<https://github.com/thymeleaf/thymeleaf-extras-springsecurity>

<https://www.baeldung.com/spring-security-thymeleaf>

## 3. Other Resources

### a. Spring Boot Lesson Playlist:

<https://youtube.com/playlist?list=PLF2cRAAApyfA6tiQrl_5WuOsk-r6syNky>

### b. ModelMapper

> I did not include ModelMapper in this project.
> Feel free to read the documentation and the teacher's slide for instructions.
<http://modelmapper.org/getting-started/>


<br>

## Happy Coding!

<br>
