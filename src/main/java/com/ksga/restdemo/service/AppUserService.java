package com.ksga.restdemo.service;

import com.ksga.restdemo.model.appuser.AppUserRequest;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AppUserService extends UserDetailsService {

    void insert(AppUserRequest appUserRequest);
}
