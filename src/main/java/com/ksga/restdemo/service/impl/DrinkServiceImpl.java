package com.ksga.restdemo.service.impl;

import com.ksga.restdemo.model.drink.Drink;
import com.ksga.restdemo.model.drink.DrinkRequest;
import com.ksga.restdemo.model.drink.DrinkUpdateRequest;
import com.ksga.restdemo.repository.DrinkRepository;
import com.ksga.restdemo.service.DrinkService;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DrinkServiceImpl implements DrinkService {

    private final DrinkRepository drinkRepository;

    public DrinkServiceImpl(DrinkRepository drinkRepository) {
        this.drinkRepository = drinkRepository;
    }

    @Override
    public List<Drink> findAll(Integer page, Integer size) {
        return drinkRepository.findAll(page, size);
    }

    @Override
    public Drink findById(Integer id) {
        return drinkRepository.findById(id);
    }

    @Override
    public Integer addDrink(DrinkRequest drinkRequest) {
        return drinkRepository.save(drinkRequest);
    }

    @Override
    public void remove(Integer id) throws NotFoundException {
        if (drinkRepository.findById(id) == null) {
            throw new NotFoundException("Cannot delete drink that does not exist");
        }
        drinkRepository.delete(id);
    }

    @Override
    public Integer update(DrinkUpdateRequest drink) throws NotFoundException {
        if (drinkRepository.findById(drink.getId()) == null) {
            throw new NotFoundException("Cannot updated drink that does not exist");
        }
        return drinkRepository.update(drink);
    }
}
