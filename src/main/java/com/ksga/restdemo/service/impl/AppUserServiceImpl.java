package com.ksga.restdemo.service.impl;

import com.ksga.restdemo.model.appuser.AppUser;
import com.ksga.restdemo.model.appuser.AppUserRequest;
import com.ksga.restdemo.repository.AppUserRepository;
import com.ksga.restdemo.service.AppUserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AppUserServiceImpl implements AppUserService {

    private final AppUserRepository appUserRepository;
    private final PasswordEncoder passwordEncoder;

    public AppUserServiceImpl(AppUserRepository appUserRepository, PasswordEncoder passwordEncoder) {
        this.appUserRepository = appUserRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void insert(AppUserRequest appUserRequest) {
        // password: 123
        String encryptedPassword = passwordEncoder.encode(appUserRequest.getPassword());

        // password: $2.dsfkjalkejdjf;lkasjf;ldskajfa;ldskj
        appUserRequest.setPassword(encryptedPassword);
        appUserRepository.insert(appUserRequest);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        AppUser user = appUserRepository.getAppUserByEmail(email);
        System.out.println(user);
        return user;
    }
}
