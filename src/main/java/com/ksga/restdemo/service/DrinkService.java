package com.ksga.restdemo.service;

import com.ksga.restdemo.model.drink.Drink;
import com.ksga.restdemo.model.drink.DrinkRequest;
import com.ksga.restdemo.model.drink.DrinkUpdateRequest;
import org.apache.ibatis.javassist.NotFoundException;

import java.util.List;

public interface DrinkService {
    List<Drink> findAll(Integer page, Integer size);

    Drink findById(Integer id);

    Integer addDrink(DrinkRequest drinkRequest);

    void remove(Integer id) throws NotFoundException;

    Integer update(DrinkUpdateRequest drink) throws NotFoundException;
}
