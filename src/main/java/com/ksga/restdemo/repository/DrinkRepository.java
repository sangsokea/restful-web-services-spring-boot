package com.ksga.restdemo.repository;

import com.ksga.restdemo.model.drink.Drink;
import com.ksga.restdemo.model.drink.DrinkRequest;
import com.ksga.restdemo.model.drink.DrinkUpdateRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DrinkRepository {

    @Select("SELECT * FROM drinks LIMIT #{size} OFFSET #{size} * (#{page} - 1)")
    List<Drink> findAll(Integer page, Integer size);

    @Select("SELECT * FROM drinks WHERE id = #{drinkId}")
    Drink findById(Integer drinkId);

    @Select("INSERT INTO drinks (name, price) " +
            "VALUES (#{drink.name}, #{drink.price}) " +
            "RETURNING id")
    Integer save(@Param("drink") DrinkRequest drinkRequest);

    @Delete("DELETE FROM drinks WHERE id = #{drinkId}")
    void delete(Integer drinkId);

    @Select("UPDATE drinks " +
            "SET name = #{drink.name}," +
            "price = #{drink.price} " +
            "WHERE id = #{drink.id} " +
            "RETURNING id")
    Integer update(@Param("drink") DrinkUpdateRequest drink);
}
