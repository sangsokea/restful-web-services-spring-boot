package com.ksga.restdemo.repository;

import com.ksga.restdemo.model.appuser.AppUser;
import com.ksga.restdemo.model.appuser.AppUserRequest;
import com.ksga.restdemo.model.appuser.UserRole;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AppUserRepository {

    @Select("SELECT *, id as user_id FROM app_users WHERE email = #{email}")
    @Result(property = "isEnabled", column = "is_enabled")
    @Result(property = "isLocked", column = "is_locked")
    @Result(property = "userRoles", column = "user_id",
            many = @Many(select = "getUserRoleByIUserId"))
    AppUser getAppUserByEmail(@Param("email") String email);

    @Insert("INSERT INTO app_users (name, email, password) " +
            "VALUES(#{request.name},#{request.email},#{request.password})")
    void insert(@Param("request") AppUserRequest appUserRequest);

    @Select("select r.id, r.role_name " +
            "from user_roles ur " +
            "inner join roles r " +
            "on r.id = ur.role_id " +
            "where ur.user_id = #{userId};")
    @Result(property = "roleName", column = "role_name")
    List<UserRole> getUserRoleByIUserId(Integer userId);
}
