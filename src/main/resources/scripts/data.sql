INSERT INTO public.drinks (id, name, price) VALUES (1, 'Latte', 1.4);
INSERT INTO public.drinks (id, name, price) VALUES (2, 'Cappuccino', 1.5);
INSERT INTO public.drinks (id, name, price) VALUES (3, 'Mocha', 1.8);
INSERT INTO public.drinks (id, name, price) VALUES (6, 'Americano', 1);
INSERT INTO public.drinks (id, name, price) VALUES (7, 'Espresso', 1);
INSERT INTO public.drinks (id, name, price) VALUES (9, 'Jasmin Tea', 3.5);

-- INSERT INTO public.app_users (id, name, email, password, is_enabled, is_locked) VALUES (2, 'Dayan', 'eam.dayan@gmail.com', '$2a$10$d3FNg82XIbty8QSpOv7A/.0cTwCrb5q1fVrhGP.Q2Lg8iQJV9dFVm', true, false);
INSERT INTO public.app_users (id, name, email, password, is_enabled, is_locked) VALUES (3, 'Kimlang', 'kimlang@gmail.com', '$2a$10$.ZGFRD5oIziV7SUdJfdJU.S90RkyIp1bOEgKyBEVuKZLR2WbAephO', true, false);
INSERT INTO public.app_users (id, name, email, password, is_enabled, is_locked) VALUES (4, 'SeavPinh', 'seavpinh@gmail.com', '$2a$10$zhz/KJTK6Wc.Wda/ZGp/yOyNsEd.26JeMdpSd.XS4aOWIHXz6SS.K', true, false);
