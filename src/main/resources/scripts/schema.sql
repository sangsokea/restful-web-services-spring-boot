create table if not exists drinks
(
    id    serial not null primary key,
    name  varchar(50)   not null,
    price real   not null
);
create table roles
(
    id        serial primary key,
    role_name varchar(50) not null unique
);

create table app_users
(
    id         serial primary key,
    name       varchar(50) not null,
    email      varchar(50) not null unique,
    password   varchar(200) not null,
    is_enabled bool not null default true,
    is_locked  bool not null default false
);

create table user_roles
(
    user_id integer references app_users (id),
    role_id integer references roles (id),
    primary key (user_id, role_id)
);