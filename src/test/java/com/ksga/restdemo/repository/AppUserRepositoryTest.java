package com.ksga.restdemo.repository;

import com.ksga.restdemo.model.appuser.AppUser;
import com.ksga.restdemo.model.appuser.AppUserRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Reference
 * @link <a href="https://linuxtut.com/en/721f9b6bd9ff841f6847/">Resource for config</a>
 */
@SpringBootTest
@TestPropertySource(locations = "classpath:persistence-entity.properties")
public class AppUserRepositoryTest {

    @Autowired
    AppUserRepository appUserRepository;

    @Test
    @DirtiesContext
    void shouldHaveValidUserDetails(){
        // given: set data to use
        appUserRepository.insert(new AppUserRequest("dayan", "eam.dayan@gmail.com", "123"));

        // when: some action
        AppUser user = appUserRepository
                .getAppUserByEmail("eam.dayan@gmail.com");

        System.out.println(user);

        // then: expected results
        assertThat(user).isNotNull();
        assertThat(user.getEmail()).isEqualTo("eam.dayan@gmail.com");
        assertThat(user.getUserRoles()).isNotNull();
        assertThat(user.getIsLocked()).isNotNull();
        assertThat(user.getIsEnabled()).isNotNull();

        user.getUserRoles().forEach(role -> {
            assertThat(role).isNotNull();
            assertThat(role.getRoleName()).isNotNull();
        });
    }
}
